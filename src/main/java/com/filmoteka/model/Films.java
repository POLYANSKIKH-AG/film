package com.filmoteka.model;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "films")
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "jsonId")
public class Films extends GenericModel {
    @Id
    @Column(name = "id",nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "default_gen")
    private long id;
    @Column(name = "title",nullable = false)
    private String title;
    @Column(name = "premier_year",nullable = false)
    private LocalDate premierYear;
    @Column(name = "country",nullable = false)
    private String country;
    @Column(name = "genre",nullable = false)//пока на стринге
    private String genre;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "film_directors",
            joinColumns = @JoinColumn(name = "film_id"), foreignKey = @ForeignKey(name = "film->director"),
            inverseJoinColumns = @JoinColumn(name = "director_id"), inverseForeignKey = @ForeignKey(name = "director->film")
    )
    //@JsonBackReference
    private Set<Directors> directors;


    @OneToMany(mappedBy = "film",  fetch = FetchType.LAZY)
    private List<Orders> orders;
}


