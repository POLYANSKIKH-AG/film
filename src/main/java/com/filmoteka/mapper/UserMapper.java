package com.filmoteka.mapper;


import com.filmoteka.DTO.UserDTO;
import com.filmoteka.Repository.OrderRepository;
import com.filmoteka.model.GenericModel;

import com.filmoteka.model.Users;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<Users, UserDTO> {
    private final OrderRepository OrderRepository;


    protected UserMapper(ModelMapper modelMapper, OrderRepository orderRepository) {
        super(modelMapper, Users.class, UserDTO.class);
        this.OrderRepository = orderRepository;


    }

    protected void setupMapper() {
        modelMapper.createTypeMap(Users.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setOrdersId)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(UserDTO.class, Users.class)
                .addMappings(m -> m.skip(Users::setOrders))
                .setPostConverter(toEntityConverter());

    }



    @Override
    protected void mapSpecificFields(UserDTO source, Users destination) {
        if (!Objects.isNull(source.getOrdersId())) {
            destination.setOrders(new HashSet<>(OrderRepository.findAllById(source.getOrdersId())));
        }
    }

    @Override
    protected void mapSpecificFields(Users source, UserDTO destination) {
        destination.setOrdersId(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Users entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getOrders())
                ? null
                : entity.getOrders()
                .stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());

    }
}
