package com.filmoteka.mapper;

import com.filmoteka.DTO.GenericDTO;
import com.filmoteka.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {

    E toEntity(D dto);

    List<E> toEntities(List<D> dtos);

    D toDto(E entity);

    List<D> toDtos(List<E> entities);
}

