package com.filmoteka.mapper;

import com.filmoteka.DTO.RoleDTO;

import com.filmoteka.model.Role;

import org.modelmapper.ModelMapper;

import java.util.Set;

public class RoleMapper extends GenericMapper<Role, RoleDTO> {

    protected RoleMapper(ModelMapper modelMapper) {
        super(modelMapper, Role.class, RoleDTO.class);
    }

    @Override
    protected void mapSpecificFields(RoleDTO source, Role destination) {

    }

    @Override
    protected void mapSpecificFields(Role source, RoleDTO destination) {

    }

    @Override
    protected Set<Long> getIds(Role entity) {
        return null;
    }
}
