package com.filmoteka.mapper;

import com.filmoteka.DTO.DirectorDTO;
import com.filmoteka.Repository.FilmRepository;
import com.filmoteka.model.Directors;
import com.filmoteka.model.GenericModel;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DirectorMapper extends GenericMapper<Directors, DirectorDTO> {
    private final FilmRepository FilmRepository;

    protected DirectorMapper(ModelMapper modelMapper,FilmRepository filmRepository) {
        super(modelMapper, Directors.class, DirectorDTO.class);
        this.FilmRepository=filmRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Directors.class, DirectorDTO.class)
                .addMappings(m -> m.skip(DirectorDTO::setFilmsId)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(DirectorDTO.class, Directors.class)
                .addMappings(m -> m.skip(Directors::setFilms)).setPostConverter(toEntityConverter());
    }



    @Override
    protected void mapSpecificFields(DirectorDTO source, Directors destination) {
            if (!Objects.isNull(source.getFilmsId())) {
                destination.setFilms(new HashSet<>(FilmRepository.findAllById(source.getFilmsId())));
            }
        }

        @Override
        protected void mapSpecificFields(Directors source, DirectorDTO destination){
            destination.setFilmsId(getIds(source));
        }

        @Override
        protected Set<Long> getIds(Directors entity){
            return Objects.isNull(entity) || Objects.isNull(entity.getFilms())
                    ? null
                    : entity.getFilms()
                    .stream()
                    .map(GenericModel::getId)
                    .collect(Collectors.toSet());

        }



}
