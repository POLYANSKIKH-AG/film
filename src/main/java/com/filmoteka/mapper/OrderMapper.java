package com.filmoteka.mapper;

import com.filmoteka.DTO.OrderDTO;

import com.filmoteka.model.Orders;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class OrderMapper extends GenericMapper<Orders, OrderDTO> {
    protected OrderMapper(ModelMapper modelMapper) {
        super(modelMapper, Orders.class, OrderDTO.class);
    }

    @Override
    protected void mapSpecificFields(OrderDTO source, Orders destination) {

    }

    @Override
    protected void mapSpecificFields(Orders source, OrderDTO destination) {

    }

    @Override
    protected Set<Long> getIds(Orders entity) {
        return null;
    }
}
