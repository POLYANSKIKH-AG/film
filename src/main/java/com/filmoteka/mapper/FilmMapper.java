package com.filmoteka.mapper;

import com.filmoteka.DTO.FilmDTO;


import com.filmoteka.Repository.DirectorRepository;
import com.filmoteka.Repository.OrderRepository;
import com.filmoteka.model.Films;

import com.filmoteka.model.GenericModel;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapper<Films, FilmDTO> {
    private final DirectorRepository DirectorRepository;
    private final OrderRepository OrderRepository;



    protected FilmMapper(ModelMapper modelMapper, DirectorRepository directorRepository, OrderRepository orderRepository) {
        super(modelMapper, Films.class, FilmDTO.class);
        this.DirectorRepository = directorRepository;
        this.OrderRepository = orderRepository;
    }


    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Films.class, FilmDTO.class)
                .addMappings(m -> m.skip(FilmDTO::setDirectorsId))
                .addMappings(m -> m.skip(FilmDTO::setOrdersId)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(FilmDTO.class, Films.class)
                .addMappings(m -> m.skip(Films::setDirectors))
                .addMappings(m -> m.skip(Films::setOrders))
                .setPostConverter(toEntityConverter());

    }


    @Override
    protected void mapSpecificFields(FilmDTO source, Films destination) {
        if (!Objects.isNull(source.getDirectorsId())) {
            destination.setDirectors(new HashSet<>(DirectorRepository.findAllById(source.getDirectorsId())));
        }
        if (!Objects.isNull(source.getOrdersId())) {
            destination.setOrders(OrderRepository.findAllById(source.getOrdersId()));
        }
    }


    @Override
    protected void mapSpecificFields(Films source, FilmDTO destination) {
        destination.setDirectorsId(getIds(source));
        destination.setOrdersId(getIds1(source));
    }

    @Override
    protected Set<Long> getIds(Films entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getDirectors())
                ? null
                : entity.getDirectors()
                .stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());

    }
    protected List<Long> getIds1(Films entity) {
        return Objects.isNull(entity)|| Objects.isNull(entity.getOrders())
                ? null
                : entity.getOrders()
                .stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());

    }



}
