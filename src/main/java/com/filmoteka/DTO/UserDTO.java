package com.filmoteka.DTO;

import com.filmoteka.model.Orders;
import com.filmoteka.model.Role;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO extends GenericDTO {

    private String login;

    private String firstName;

    private String lastName;

    private String middleName;

    private LocalDate birthDate;

    private String phone;

    private String address;

    private String email;

    private LocalDateTime createdWhen;

    private Long roleId;

    private Set<Long> ordersId;

    private String password;

}
