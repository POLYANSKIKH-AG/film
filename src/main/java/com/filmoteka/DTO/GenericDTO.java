package com.filmoteka.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.AllArgsConstructor;

import java.time.LocalDateTime;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public abstract class GenericDTO {

    protected Long id;
    protected LocalDateTime createdWhen = LocalDateTime.now();
    protected String createdBy = "DEFAULT_USER";
}

