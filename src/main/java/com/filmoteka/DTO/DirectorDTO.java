package com.filmoteka.DTO;

import com.filmoteka.model.Films;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DirectorDTO extends GenericDTO {


    private String directorsFio;
    private String position;
    private Set<Long> filmsId;
}
