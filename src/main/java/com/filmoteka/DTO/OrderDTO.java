package com.filmoteka.DTO;

import com.filmoteka.model.Films;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO extends GenericDTO {

    private Long userId;

    private Long filmId;

    private LocalDateTime rentDate;

    private Long rentPeriod;

    private Boolean purchase;
}
