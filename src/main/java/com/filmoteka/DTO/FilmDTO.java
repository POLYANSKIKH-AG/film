package com.filmoteka.DTO;

import com.filmoteka.model.Directors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDTO extends GenericDTO {

    private String title;

    private LocalDate premierYear;

    private String country;

    private String genre;

    private Set<Long> directorsId;

    private List<Long> ordersId;



}
