package com.filmoteka.DTO;

import jakarta.persistence.Column;

public class RoleDTO extends GenericDTO {

    private String title;

    private String description;
}
