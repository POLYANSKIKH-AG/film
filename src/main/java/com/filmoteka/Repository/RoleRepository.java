package com.filmoteka.Repository;

import com.filmoteka.model.Role;

public interface RoleRepository extends GenericRepository<Role>{
    Role getRoleByTitle(String title);
}
