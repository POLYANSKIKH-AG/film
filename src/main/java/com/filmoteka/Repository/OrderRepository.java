package com.filmoteka.Repository;

import com.filmoteka.model.Orders;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends GenericRepository<Orders>{
}
