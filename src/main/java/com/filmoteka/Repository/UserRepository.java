package com.filmoteka.Repository;

import com.filmoteka.model.Users;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<Users>{

    Users findUserByLogin(String login);

    Users findUserByEmail(String email);

}
