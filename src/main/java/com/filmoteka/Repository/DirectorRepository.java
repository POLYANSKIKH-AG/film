package com.filmoteka.Repository;

import com.filmoteka.model.Directors;
import org.springframework.stereotype.Repository;


@Repository
public interface DirectorRepository extends GenericRepository<Directors> {


}
