package com.filmoteka.Repository;

import com.filmoteka.model.Films;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository extends GenericRepository<Films> {


}
