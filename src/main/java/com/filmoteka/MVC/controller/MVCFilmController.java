package com.filmoteka.MVC.controller;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.filmoteka.DTO.FilmDTO;
import com.filmoteka.mapper.FilmMapper;
import com.filmoteka.service.FilmService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/films")
public class MVCFilmController {

    private final FilmService service;
    private final FilmMapper mapper;


    public MVCFilmController(FilmService service, FilmMapper mapper) {
        this.service = service;
        this.mapper = mapper;

    }

  @GetMapping("")
  public String getAll(Model model) {
    model.addAttribute("films", mapper.toDtos(service.listAll()));
    return "Films/viewAllFilms";
  }





    @GetMapping("/add")
    public String addBook() {
        return "Films/addFilm";
    }

    @PostMapping("/add")
    public String addBook(@ModelAttribute("filmsForm") FilmDTO filmDto) throws IOException {

            service.create(mapper.toEntity(filmDto));

        return "redirect:/films";
    }









}

