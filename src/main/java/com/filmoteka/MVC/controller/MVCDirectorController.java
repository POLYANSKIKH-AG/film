package com.filmoteka.MVC.controller;

import com.filmoteka.DTO.DirectorDTO;
import com.filmoteka.DTO.FilmDTO;
import com.filmoteka.mapper.DirectorMapper;
import com.filmoteka.mapper.FilmMapper;
import com.filmoteka.service.DirectorService;
import com.filmoteka.service.FilmService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@Controller
@RequestMapping("/directors")
public class MVCDirectorController {
    private final DirectorService service;
    private final DirectorMapper mapper;


    public MVCDirectorController(DirectorService service, DirectorMapper mapper) {
        this.service = service;
        this.mapper = mapper;

    }

    @GetMapping("")
    public String getAll(Model model) {
        model.addAttribute("directors", mapper.toDtos(service.listAll()));
        return "Directors/viewAllDirectors";
    }


    @GetMapping("/add")
    public String addDirector() {
        return "Directors/addDirectors";
    }

    @PostMapping("/add")
    public String addDirector(@ModelAttribute("filmsForm") DirectorDTO directorDto) throws IOException {

        service.create(mapper.toEntity(directorDto));

        return "redirect:/directors";
    }



}
