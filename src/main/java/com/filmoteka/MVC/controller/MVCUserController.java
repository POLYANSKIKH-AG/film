package com.filmoteka.MVC.controller;

import com.filmoteka.DTO.UserDTO;
import com.filmoteka.mapper.UserMapper;
import com.filmoteka.model.Users;
import com.filmoteka.service.UserService;
import jakarta.websocket.server.PathParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.List;

import static com.filmoteka.constants.UserRolesConstants.ROLE_ADMIN;

@Controller
@RequestMapping("/users")
public class MVCUserController {

    private final UserService service;
    private final UserMapper mapper;

    public MVCUserController(UserService service, UserMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }


    @PostMapping("/registration")
    public <UserDto> String registration(@ModelAttribute("userForm") UserDTO userDto, BindingResult bindingResult) {
        if (userDto.getLogin().equals(ROLE_ADMIN) || service.getUserByLogin(userDto.getLogin()) != null) {
            bindingResult.rejectValue("login", "error.login", "Такой логин уже существует");
            return "redirect:/login";
        }
        if (service.getUserByEmail(userDto.getEmail()) != null) {
            bindingResult.rejectValue("email", "error.email", "Такая почта уже существует");
            return "redirect:/login";
        }
        service.create(mapper.toEntity(userDto));
        return "redirect:/login";
    }

    @GetMapping("/profile/{id}")
    public String viewProfile(@PathVariable Long id, Model model) {
        model.addAttribute("user", mapper.toDto(service.getOne(id)));
        return "profile/viewProfile";
    }

    @GetMapping("/profile/update/{id}")
    public String updateProfile(@PathVariable Long id, Model model) {
        model.addAttribute("user", mapper.toDto(service.getOne(id)));
        return "profile/updateProfile";
    }

    @PostMapping("/profile/update")
    public String updateProfile(@ModelAttribute("userForm") UserDTO userDto) {
        service.update(mapper.toEntity(userDto));
        return "redirect:/users/profile/" + userDto.getId();
    }

    @GetMapping("")
    public String getAll(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "5") int pageSize,
            Model model
    ) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Direction.ASC, "login"));
        Page<Users> userPage = service.listAll(pageRequest);
        Page<UserDTO> userDtoPage = new PageImpl<>(mapper.toDtos(userPage.getContent()), pageRequest, userPage.getTotalElements());
        model.addAttribute("users", userDtoPage);
        return "users/viewAllUsers";
    }
//    @GetMapping("")
//    public String getAll(Model model) {
//        model.addAttribute("users", mapper.toDtos(service.listAll()));
//        return "users/viewAllUsers";
//    }



}