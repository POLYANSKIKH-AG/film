package com.filmoteka.service;

import com.filmoteka.Repository.DirectorRepository;
import com.filmoteka.model.Directors;
import org.springframework.stereotype.Service;


@Service
public class DirectorService extends GenericService<Directors> {

    private final DirectorRepository directorRepository;


    public DirectorService(DirectorRepository directorRepository) {
        super(directorRepository);
        this.directorRepository = directorRepository;
    }
}
