package com.filmoteka.service;

import com.filmoteka.Repository.OrderRepository;
import com.filmoteka.model.Orders;
import org.springframework.stereotype.Service;

@Service
public class OrderService extends GenericService<Orders>{
    public final OrderRepository orderRepository;

    protected OrderService(OrderRepository orderRepository) {
        super(orderRepository);
        this.orderRepository = orderRepository;
    }
}
