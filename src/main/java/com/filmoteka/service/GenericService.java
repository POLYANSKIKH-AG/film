package com.filmoteka.service;

import com.filmoteka.Repository.GenericRepository;
import com.filmoteka.model.GenericModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;


@Service
public abstract class GenericService<T extends GenericModel> {
    private final GenericRepository<T> GenericRepository;


    protected GenericService(com.filmoteka.Repository.GenericRepository<T> genericRepository) {
        this.GenericRepository = genericRepository;
    }



    public List<T> listAll() {
        return GenericRepository.findAll();
    }
    public Page<T> listAll(Pageable pageable) {
        return GenericRepository.findAll(pageable);
    }


    public T getOne(Long id) {
        return GenericRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Запись с таким id не найдена"));
    }

    public T create(T object) {
        object.setCreatedBy("ADMIN");
        object.setCreatedWhen(LocalDateTime.now());
        return GenericRepository.save(object);
    }

    public T update(T object) {
        return GenericRepository.save(object);
    }

    public void delete(Long id) {
        GenericRepository.deleteById(id);
    }















}
