package com.filmoteka.service;

import com.filmoteka.Repository.GenericRepository;
import com.filmoteka.Repository.RoleRepository;
import com.filmoteka.model.Role;
import org.springframework.stereotype.Service;

@Service
public class RoleService extends GenericService<Role>{
    private final RoleRepository roleRepository;
    protected RoleService(com.filmoteka.Repository.GenericRepository<Role> genericRepository,RoleRepository roleRepository) {
        super(genericRepository);
        this.roleRepository = roleRepository;
    }

    public Role getByTitle(String title) {
        return roleRepository.getRoleByTitle(title);
    }

}
