package com.filmoteka.service;

import com.filmoteka.Repository.DirectorRepository;
import com.filmoteka.Repository.UserRepository;
import com.filmoteka.model.Users;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static com.filmoteka.constants.UserRolesConstants.ROLE_USER;

@Service
public class UserService extends GenericService<Users> {
    private final UserRepository userRepository;
    private final RoleService roleService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    public UserService(UserRepository userRepository, RoleService roleService,BCryptPasswordEncoder bCryptPasswordEncoder) {
        super(userRepository);
        this.userRepository = userRepository;
        this.roleService = roleService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }



    public Users getUserByLogin(String login) {
        return userRepository.findUserByLogin(login);
    }

    public Users getUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }


    public Users create(Users object) {
        object.setRole(roleService.getByTitle(ROLE_USER));
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setCreatedWhen(LocalDateTime.now());
        object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
        return super.create(object);
    }

    @Override
    public Users update(Users object) {
        Users foundUser = getOne(object.getId());
        object.setRole(foundUser.getRole());
        object.setPassword(foundUser.getPassword());
        object.setCreatedBy(foundUser.getCreatedBy());
        object.setCreatedWhen(foundUser.getCreatedWhen());
        return super.update(object);
    }


    public boolean checkPassword(String password, UserDetails foundUser) {
        return bCryptPasswordEncoder.matches(password, foundUser.getPassword());
    }






}
