package com.filmoteka.service;

import com.filmoteka.Repository.FilmRepository;
import com.filmoteka.model.Films;
import org.springframework.stereotype.Service;

@Service
public class FilmService extends GenericService<Films>{
    private final FilmRepository filmRepository;

    public FilmService(FilmRepository filmRepository) {
        super(filmRepository);
        this.filmRepository = filmRepository;
    }
}
