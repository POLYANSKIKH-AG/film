package com.filmoteka.controller;


import com.filmoteka.DTO.RoleDTO;
import com.filmoteka.model.Role;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "role", description = "Контроллер для работы с ролями")
@RestController
@RequestMapping("/Role")
@Slf4j
public class RoleController extends GenericController<Role, RoleDTO> {



}
