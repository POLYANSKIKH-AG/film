package com.filmoteka.controller;


import com.filmoteka.DTO.FilmDTO;
import com.filmoteka.mapper.FilmMapper;
import com.filmoteka.model.Directors;
import com.filmoteka.model.Films;
import com.filmoteka.service.DirectorService;
import com.filmoteka.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;

@Tag(name = "фильм", description = "Контроллер для работы с фильмами")
@RestController
//@RequestMapping("/rest/films")
@RequestMapping("/films")
@Slf4j
public class FilmController extends GenericController<Films, FilmDTO> {
    private final DirectorService DirectorService;
    private final FilmService FilmService;
    private final FilmMapper FilmMapper;
    public FilmController(@Autowired FilmService filmService,DirectorService directorService,FilmMapper filmMapper){
        super(filmService,filmMapper);
        this.DirectorService=directorService;
        this.FilmService=filmService;
        this.FilmMapper=filmMapper;
    }


    @Operation(description = "Добавить режисера к фильму", method = "addDir")
    @RequestMapping(value = "/addDir", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDTO> addFilm(@RequestParam(value = "filmId") Long filmId,
                                             @RequestParam(value = "directorId") Long directorId) {
        try {
            Films Film = FilmService.getOne(filmId);
            Directors Director = DirectorService.getOne(directorId);
            Film.getDirectors().add(Director);
            FilmService.create(Film);
            return ResponseEntity.status(HttpStatus.OK).body(FilmMapper.toDto(Film));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }

    }


}
