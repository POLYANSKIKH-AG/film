package com.filmoteka.controller;

import com.filmoteka.DTO.DirectorDTO;
import com.filmoteka.mapper.DirectorMapper;
import com.filmoteka.model.Directors;
import com.filmoteka.model.Films;
import com.filmoteka.service.DirectorService;
import com.filmoteka.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;


@Tag(name = "Директор", description = "Контроллер для работы с директорами")
@RestController
//@RequestMapping("/rest/directors")
@RequestMapping("/directors")
@Slf4j
public class DirectorController extends GenericController<Directors, DirectorDTO> {
    //private final FilmRepository FilmRepository;
    private final DirectorService DirectorService;
    private final FilmService FilmService;
    private final DirectorMapper DirectorMapper;


    public DirectorController(@Autowired /*DirectorRepository DirectorRepository, FilmRepository filmRepository, */
                                      DirectorService directorService, FilmService filmService, DirectorMapper directorMapper){
        super(directorService,directorMapper);
     //   this.FilmRepository = filmRepository;
        this.DirectorService = directorService;
        this.FilmService = filmService;
        this.DirectorMapper = directorMapper;
    }

    @Operation(description = "Добавить фильм к режисеру", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDTO> addFilm(@RequestParam(value = "filmId") Long filmId,
                                          @RequestParam(value = "directorId") Long directorId) {
        try {
            Films film = FilmService.getOne(filmId);
            Directors Director = DirectorService.getOne(directorId);
            Director.getFilms().add(film);
            DirectorService.create(Director);
            return ResponseEntity.status(HttpStatus.OK).body(DirectorMapper.toDto(Director));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }







}