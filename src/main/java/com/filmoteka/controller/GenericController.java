package com.filmoteka.controller;
import com.filmoteka.DTO.GenericDTO;
import com.filmoteka.Repository.GenericRepository;
import com.filmoteka.mapper.GenericMapper;
import com.filmoteka.model.GenericModel;
import com.filmoteka.service.GenericService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;


@RequiredArgsConstructor
@Slf4j
@RestController
@SecurityRequirement(name = "Bearer Authentication")
public abstract class GenericController<T extends GenericModel, N extends GenericDTO> {

    private GenericService<T> GenericService;
    //private GenericRepository<T> genericRepository;
    private GenericMapper<T,N> GenericMapper;


    public GenericController(GenericService<T> GenericService, GenericMapper<T,N> GenericMapper) {
        this.GenericService = GenericService;
        this.GenericMapper = GenericMapper;
    }


/*
    @Operation(description = "Получить запись по ID", method = "getById")
    @RequestMapping(value = "/getById", method = RequestMethod.GET,
            produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<T> getById(@RequestParam(value = "id") Long id) {
        return genericRepository.findById(id)
                .map(t -> ResponseEntity.status(HttpStatus.OK).body(t))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }
*/
    @Operation(description = "Получить запись по ID", method = "getById")
    @RequestMapping(value = "/getById", method = RequestMethod.GET,
            produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<N> getById(@RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(GenericMapper.toDto(GenericService.getOne(id)));
    }


/*
    @Operation(description = "Получить все записи", method = "getAll")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<T>> getAll() {
        return ResponseEntity.ok(genericRepository.findAll());
    }
*/
    @Operation(description = "Получить все записи", method = "getAll")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<N>> getAll() {
        return ResponseEntity.ok(GenericMapper.toDtos(GenericService.listAll()));
    }



//    @Operation(description = "Создать новую запись", method = "create")
//    @RequestMapping(value = "/add", method = RequestMethod.POST,
//            produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
//    public ResponseEntity<T> create(@RequestBody T newEntity) {
//        if (newEntity.getId() != 0 && genericRepository.existsById(newEntity.getId())) {
//            return ResponseEntity.status(HttpStatus.CONFLICT).build();
//        }
//        return ResponseEntity
//                .status(HttpStatus.CREATED)
//                .body(genericRepository.save(newEntity));
//    }

    @Operation(description = "Создать новую запись", method = "create")
    @RequestMapping(value = "/add", method = RequestMethod.POST,
            produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<T> create(@RequestBody N newEntity) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(GenericService.create(GenericMapper.toEntity(newEntity)));
    }






//    @Operation(description = "Обновить запись", method = "update")
//    @RequestMapping(value = "/update", method = RequestMethod.PUT,
//            produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
//    public ResponseEntity<T> update(@RequestBody T updatedEntity,
//                                    @RequestParam(value = "id") Long id) {
//        updatedEntity.setId(id);
//        return ResponseEntity
//                .status(HttpStatus.CREATED)
//                .body(genericRepository.save(updatedEntity));
//    }
@Operation(description = "Обновить запись", method = "update")
    @RequestMapping(value = "/update", method = RequestMethod.PUT,
            produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<N> update(@RequestBody T updatedEntity,
                                    @RequestParam(value = "id") Long id) {
        updatedEntity.setId(id);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(GenericMapper.toDto(GenericService.update(updatedEntity)));
    }


//    @Operation(description = "Удалить запись по ID", method = "delete")
//    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
//    public void delete(@PathVariable(value = "id") Long id) {
//        genericRepository.deleteById(id);
//    }


    @Operation(description = "Удалить запись по ID", method = "delete")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        GenericService.delete(id);
    }






}

