package com.filmoteka.controller;


import com.filmoteka.Config.jwt.JWTTokenUtil;
import com.filmoteka.DTO.LoginDTO;
import com.filmoteka.DTO.UserDTO;
import com.filmoteka.Repository.UserRepository;
import com.filmoteka.mapper.UserMapper;
import com.filmoteka.model.Users;
import com.filmoteka.service.UserService;
import com.filmoteka.service.userdetails.CustomUserDetailsService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Tag(name = "пользователь", description = "Контроллер для работы с пользователями")
@RestController
//@RequestMapping("/rest/user")
@RequestMapping("/user")
@Slf4j
public class UserController extends GenericController<Users, UserDTO> {

    private final CustomUserDetailsService customUserDetailsService;
    private final JWTTokenUtil jwtTokenUtil;
    private final UserService userService;



    public UserController(@Autowired UserService userService, UserMapper userMapper,JWTTokenUtil jwtTokenUtil,CustomUserDetailsService customUserDetailsService){
        super(userService,userMapper);
        this.jwtTokenUtil = jwtTokenUtil;
        this.customUserDetailsService = customUserDetailsService;
        this.userService = userService;
    }




    @PostMapping("/auth")
    public ResponseEntity<?> auth(@RequestBody LoginDTO loginDto) {
        Map<String, Object> response = new HashMap<>();
        UserDetails foundUser = customUserDetailsService.loadUserByUsername(loginDto.getLogin());
        if(!userService.checkPassword(loginDto.getPassword(), foundUser)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Ошибка авторизации!\n Неверный пароль!");
        }

        String token = jwtTokenUtil.generateToken(foundUser);
        response.put("token", token);
        response.put("username", foundUser.getUsername());
        response.put("authorities", foundUser.getAuthorities());
        return ResponseEntity.ok().body(response);
    }


}
