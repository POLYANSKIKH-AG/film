package com.filmoteka.controller;

import com.filmoteka.DTO.OrderDTO;
import com.filmoteka.mapper.OrderMapper;
import com.filmoteka.model.Directors;
import com.filmoteka.model.Orders;
import com.filmoteka.service.FilmService;
import com.filmoteka.service.OrderService;
import com.filmoteka.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;

@Tag(name = "заказ", description = "Контроллер для работы с заказами")
@RestController
//@RequestMapping("/rest/orders")
@RequestMapping("/orders")
@Slf4j
public class OrderController extends GenericController<Orders, OrderDTO> {
    private final OrderService OrderService;
    private final FilmService FilmService;
    private final UserService UserService;
    private final OrderMapper OrderMapper;

    public OrderController(@Autowired OrderService orderService, UserService userService, FilmService filmService, com.filmoteka.mapper.OrderMapper orderMapper){
        super(orderService,orderMapper);
        this.FilmService = filmService;
        this.UserService = userService;
        this.OrderService = orderService;
        this.OrderMapper=orderMapper;
    }

    @Operation(description = "создать заказ", method = "createOrder")
    @RequestMapping(value = "/createOrder", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Orders> addOrder(@RequestParam(value = "filmId") Long filmId,
                                           @RequestParam(value = "userId") Long userId,@RequestParam(value = "rentPeriod") Long rentPeriod){
        try {
            List<Orders> orders = OrderService.listAll();
            Orders newOrder = new Orders();
            newOrder.setId(orders.size()+1);
            newOrder.setFilm(FilmService.getOne(filmId));
            newOrder.setUser(UserService.getOne(userId));
            newOrder.setRentDate(LocalDateTime.now());
            newOrder.setRentPeriod(rentPeriod);
            newOrder.setPurchase(true);

            OrderService.create(newOrder);
        return ResponseEntity.status(HttpStatus.OK).body(OrderService.create(newOrder));
         }catch (
            NotFoundException e){
        return ResponseEntity.notFound().build();
         }

    }





}
