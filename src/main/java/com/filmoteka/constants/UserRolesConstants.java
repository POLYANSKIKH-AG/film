package com.filmoteka.constants;

public interface UserRolesConstants {
    String ROLE_ADMIN = "ROLE_ADMIN";

    String ROLE_USER = "ROLE_USER";

}

